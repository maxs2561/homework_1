﻿#include <iostream>
#include <Windows.h>

using namespace std;

int main()
{
	int i = 3;
	string name;

	cout << "Enter your name:";
	cin >> name;

	for (; i >= 0; i--)
	{
		cout << "\nWaiting for " << i << " seconds...";
		Sleep(1000);
		system("cls");
	}

	cout << "Hello, " << name << "!" << endl;

	return 0;
}